﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GPPR
{
    public class FPS_Mouse : MonoBehaviour          //written by Leo
    {
        public float sensitivity = 100f;

        public Transform playBod;

        [SerializeField]
        Slider sensitivitySlider;

        float xRotation = 0f;

        float mx, my;

        public bool clamp = true;

        Camera cam;

        float orignalFOV, originalSensitivity;

        public bool itemChosen;
        bool canRotate;
        bool setSensitivityBackOnce;

        void Start()
        {
            lockMouse();
            if (transform.parent) if (playBod == null) playBod = transform.parent;
            cam = Camera.main;


            sensitivity = PlayerPrefs.GetInt("sensitivityValue", 100);
            if (sensitivitySlider != null) sensitivitySlider.value = PlayerPrefs.GetInt("sensitivityValue", 100);



            orignalFOV = cam.fieldOfView;
            originalSensitivity = sensitivity;
        }

        public void lockMouse()
        {
            Cursor.lockState = CursorLockMode.Locked;
            canRotate = true;
        }

        public void unlockMouse()
        {
            Cursor.lockState = CursorLockMode.None;
            canRotate = false;
        }

        public void changeSensitivity()
        {
            sensitivity = sensitivitySlider.value;
            PlayerPrefs.SetInt("sensitivityValue", (int)sensitivitySlider.value);
            PlayerPrefs.Save();

        }

        void Update()
        {

            if (!itemChosen)
            {
                mx = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
                my = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;
            }
            else
            {
                mx = 0;
                my = 0;
            }

            if (canRotate)
            {
                xRotation -= my;
                if (clamp) xRotation = Mathf.Clamp(xRotation, -90f, 90f);

                transform.localRotation = Quaternion.Euler(xRotation, 0f, transform.eulerAngles.z);
                if (playBod != null) playBod.Rotate(Vector3.up * mx);
            }

            if (Input.GetMouseButton(1))
            {
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, orignalFOV * 0.5f, 0.2f);
                sensitivity = originalSensitivity / 2;
                setSensitivityBackOnce = true;
            }
            else
            {
                if (setSensitivityBackOnce)
                {
                    sensitivity = originalSensitivity;
                    setSensitivityBackOnce = false;
                }
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, orignalFOV, 0.2f);
            }
        }
    }
}

