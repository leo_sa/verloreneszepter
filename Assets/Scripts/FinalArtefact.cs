﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class FinalArtefact : MonoBehaviour      //written by Leo
    {
        [SerializeField]
        GameObject finalStatue;
        [SerializeField]
        GameObject fadeOutScreen, dreiZack;

        GameObject localStatue;

        Transform statuePoint;

        [SerializeField]
        AudioSource voiceLineSource;
        AudioSource music;

        Material material_;

        [SerializeField]
        AudioMixer mixer;

        [SerializeField]
        LoadSceneMode loadscene;

        [SerializeField]
        float finalVoiceLength;

        float emmission_Intensity;

        float mixerVolume, internVolume;

        [SerializeField]
        string levelName;

        [HideInInspector]
        public bool decreaseVolume;         //public for ArtefactManager


        private void Start()
        {
            material_ = dreiZack.GetComponent<Renderer>().material;
            music = Camera.main.GetComponent<AudioSource>();
        }

        private void Update()
        {
            //if (Input.GetKeyDown("f")) remove();          //uncomment for debug purposes
            if (decreaseVolume)
            {
                internVolume = Mathf.Lerp(mixerVolume, -250, 0.01f);         //-250 so it gets muted or silent enough for sure
                mixer.SetFloat("AmbienceVolume", internVolume);
                music.volume = Mathf.Lerp(music.volume, 0, 0.05f);      //also slowly muting the music audiosource
            }
            if (emmission_Intensity > 0)
            {
                emmission_Intensity = Mathf.Lerp(emmission_Intensity, 0, 0.2f);
                material_.SetColor("_EmissionColor", new Color(0, 165, 191) * emmission_Intensity);
            }
        }


        public void remove()                //activated when player interacts with it
        {
            if (voiceLineSource.isPlaying) voiceLineSource.Stop();          //stop a voiceline if it is playing, so only ONE voiceline is playing at time
            decreaseVolume = true;
            Invoke("endGame", finalVoiceLength);
            dreiZack.SetActive(true);

            emmission_Intensity = 1;
            material_.EnableKeyword("_EmissionColor");
            material_.SetColor("_EmissionColor", new Color(0, 165, 191) * emmission_Intensity);
            material_.EnableKeyword("_EmissionColor");

            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Collider>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }

        void endGame()             //Instantiates fade out screen and invokes sceeneload with delay to give "fade out time".
        {
            Instantiate(fadeOutScreen, transform.position, Quaternion.identity);
            Invoke("loadScene", 2);
        }

        void loadScene()
        {
            PlayerPrefs.SetInt("endGame", 1);               //could also be used with a custom static class, but this reference was faster. It is used to determine if the player enters the credits before the end of the game or after.
            SceneManager.LoadScene(levelName, loadscene);
        }
    }

}
