﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class Artefact : MonoBehaviour       //written by Leo
    {
        ArtefactManager artefactManager;

        ParticleSystem thisParticlesystem;

        private void Start()
        {
            if (transform.parent.transform.parent) if (transform.parent.transform.parent.GetComponentInChildren<ParticleSystem>()) thisParticlesystem = transform.parent.transform.parent.GetComponentInChildren<ParticleSystem>();
            artefactManager = FindObjectOfType<ArtefactManager>();
            artefactManager.artefacts.Add(gameObject);          //add it to the list in the beginning
        }

        public void remove()
        {
            if (thisParticlesystem != null) thisParticlesystem.loop = false;        //if removed, particle system needs to "loop out"
            artefactManager.playAudio(artefactManager.artefacts.Count);             //when removed, subtract it drom te list
            artefactManager.artefacts.Remove(gameObject);
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name == "Player") remove();        //remove on Player collision
        }
    }

}
