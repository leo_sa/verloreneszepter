﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GPPR
{
    public class VolumeSlider : MonoBehaviour       //written by Leo
    {
        [SerializeField]
        Slider slider;

        private void Start()
        {
            slider.value = PlayerPrefs.GetFloat("AudioLis", 1);
        }

        public void changeAudioVolume()     //change the Volume to the interactable slider value.
        {
            PlayerPrefs.SetFloat("AudioLis", slider.value);
            PlayerPrefs.Save();

            AudioListener.volume = slider.value;
        }
    }
}

