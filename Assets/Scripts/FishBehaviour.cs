﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class FishBehaviour : MonoBehaviour          //written by Khira
    {
        // class that moves fish with random speed

        // two numbers to set a random speed for the fish inbetween those, speed with that the fish rotates, the distance to other fish (in this distance they will notice each other)
        public float fishSpeedFast, fishSpeedSlow, rotationSpeed, distanceToOthers;
        float fishSpeed;
        // Vector that gives the average direction of the whole group of fish, and average position of the group (middle of the group)
        Vector3 averageHeadingDirection, averagePosition;
        // bool to turn fish around, when it gets too far from group
        bool turn = false;
        public GameObject MisOfTank;

        // Start is called before the first frame update
        void Start()
        {
            // fishSpeed is set to a random number between both speeds that can be set in editor
            fishSpeed = Random.Range(fishSpeedSlow, fishSpeedFast);
        }

        // Update is called once per frame
        void Update()
        {
            if (Vector3.Distance(transform.position, MisOfTank.transform.position) >= GlobalFisch.tankSize)
            {
                turn = true;
            }
            else
            {
                turn = false;
            }
            if (turn)
            {
                Debug.Log("Turning.");
                Vector3 direction = MisOfTank.transform.position - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                      Quaternion.LookRotation(direction),
                                                      rotationSpeed * Time.deltaTime);
                fishSpeed = Random.Range(fishSpeedSlow, fishSpeedFast);
            }
            else
            {
                // is done every 1 in 5 times
                if (Random.Range(0, 5) < 1)
                {
                    //Debug.Log("Rules are Applied.");
                    ApplyRules();
                }
            }

            transform.Translate(0, 0, Time.deltaTime * fishSpeed);
        }
        public void ApplyRules()
        {
            GameObject[] allMyFish;
            allMyFish = GlobalFisch.allFish;
            Vector3 center = Vector3.zero;
            Vector3 avoid = Vector3.zero;
            Vector3 goalPosition = GlobalFisch.goalPosition;
            float distance;
            float groupSpeed = 0.1f;
            int groupSize = 0;

            //for every fish in our tank, do something
            foreach (GameObject fish in allMyFish)
            {
                if (fish != this.gameObject)
                {
                    //Debug.Log("This fish is not us.");
                    // the distance between our current fish with this script and another fish in tank
                    distance = Vector3.Distance(fish.transform.position, this.transform.position);
                    // if the distance between out fish and another is smaller or equal the distance to others, do something
                    if (distance <= distanceToOthers)
                    {
                        //Debug.Log("We are neighbours.");
                        // add the position of the fish to the centre, so it stays in the middle of the group, and add +1 to the group size
                        center += fish.transform.position;
                        groupSize++;

                        // if the distance between our fish and another fish is smaller than one, try to avoid the other fish
                        if (distance < 1f)
                        {
                            //Debug.Log("Collision should be avoided.");
                            // avoid Vector gets set to a direction turning away from the other fish
                            avoid = avoid + (this.transform.position - fish.transform.position);
                        }
                        // creates new class fish behaviour witch is set to the specific behaviour of the fish this script is on
                        FishBehaviour anotherFishBehaviour = fish.GetComponent<FishBehaviour>();
                        // add the speed of the current fish to the speed of the whole group of fish and make this the new group speed
                        groupSpeed = groupSpeed + anotherFishBehaviour.fishSpeed;
                    }
                }
            }

            // if the group size is bigger than 0, do something
            if (groupSize > 0)
            {
                //print("groupSize " + groupSize);
                // change the center of the group to (mid value of center + way/direction for this fish to goal position)
                center = center / groupSize + (goalPosition - this.transform.position);
                // fish speed is the average of the whole groups speed divided by size of group
                fishSpeed = groupSpeed / groupSize;
                // direction is the new position of center added to the vector to avoid other fish minus the position of our fish
                Vector3 direction = (center + avoid) - transform.position;
                // if direction is not 0, do something
                if (direction != Vector3.zero)
                {
                    // rotate the fish by direction and with the speed of rotation Speed
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
                }
            }
        }
    }
}

