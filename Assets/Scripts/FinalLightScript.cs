﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class FinalLightScript : MonoBehaviour           //written by Leo
    {
        Light light;
        Transform playerTransform;

        [SerializeField]
        bool enableRange;

        [SerializeField]
        float multiplierIntensity = 1, multiplierRange = 1;

        float intensity;


        private void Start()
        {
            light = GetComponent<Light>();
            playerTransform = GameObject.Find("Player").transform;
        }


        void Update()
        {
            Mathf.Clamp(intensity, 0, 300);
            light.intensity = Vector3.Distance(transform.position, playerTransform.position) * multiplierIntensity;             //to disable uncomfortable brightness, the light's intensity changes with player distance
            if (enableRange)
            {
                light.range = Vector3.Distance(transform.position, playerTransform.position) * multiplierRange;
            }


        }
    }

}
