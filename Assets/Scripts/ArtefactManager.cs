﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace GPPR
{
    public class ArtefactManager : MonoBehaviour2           //written by Leo
    {
        float originalMusicValue;

        bool silenceLerp, canSetAudioBack;

        GameObject finalAFact;

        FinalArtefact finalArtefactInstance;            //reference to decrease volume

        int index;

        [SerializeField, Range(0, 1)]
        float musicSpeakVolume = 0.25f;

        float internVolume, mixerVolume;

        public List<GameObject> artefacts;

        AudioSource audioSource, musicSource;

        [SerializeField]
        AudioMixer mixer;

        [SerializeField]
        AudioClip[] voiceLines;

        [SerializeField]
        Material unEmissiveMat, emissiveMat;

        private void Start()
        {
            finalArtefactInstance = FindObjectOfType<FinalArtefact>();

            audioSource = GetComponent<AudioSource>();
            musicSource = Camera.main.GetComponent<AudioSource>();
            mixer.GetFloat("AmbienceVolume", out originalMusicValue);
            Debug.Log(originalMusicValue);

            finalAFact = GameObject.Find("FinalArtefact");
            finalAFact.GetComponent<MeshCollider>().enabled = false;
            finalAFact.GetComponent<BoxCollider>().enabled = false;
            finalAFact.GetComponent<Renderer>().material = unEmissiveMat;
            finalAFact.transform.parent.GetComponentInChildren<Light>().enabled = false;
        }

        private void Update()
        {
            mixer.GetFloat("AmbienceVolume", out mixerVolume);

            if (!finalArtefactInstance.decreaseVolume)          //this script should only interact with the audio when the final artefact does not, because the latter one has a unique method of interaction with the audio
            {
                if (!canSetAudioBack && !silenceLerp)
                {
                    internVolume = Mathf.Lerp(mixerVolume, originalMusicValue, 0.02f);
                    mixer.SetFloat("AmbienceVolume", internVolume);
                }
                else if (silenceLerp)
                {
                    if (Mathf.Abs(mixerVolume - (originalMusicValue * musicSpeakVolume)) >= 0.01f)                      //silencing it through a "smoothed" lerp, because reaching exactly originalMusicValue * musicSpeakVolume would take more unecessary frames, and an inaccuracy of 0.1f is very subtle, thus being appropriate to save those frames.
                    {
                        internVolume = Mathf.Lerp(mixerVolume, originalMusicValue * musicSpeakVolume, 0.01f);
                        mixer.SetFloat("AmbienceVolume", internVolume);
                    }
                    else
                    {
                        mixer.SetFloat("AmbienceVolume", originalMusicValue * musicSpeakVolume);
                    }

                    if (canSetAudioBack)
                    {
                        if (!audioSource.isPlaying)
                        {
                            canSetAudioBack = false;
                            silenceLerp = false;
                        }
                    }
                }
            }
        }


        public void playAudio(int i)
        {
            silenceLerp = true;
            index = i;
            Invoke("delayedPlay", 1.5f);                //audio is played with delay, because the music has to be silenced smoothly first
        }

        void delayedPlay()
        {
            audioSource.clip = voiceLines[index];
            audioSource.Play();
            canSetAudioBack = true;

            if (artefacts.Count < 1)
            {
                finalArtefactEnable();          //enabling the final artefact when every other artefact is taken
            }
        }

        void finalArtefactEnable()              //enabling the "interaction signifiers" + ability to interact on the final artefact
        {
            finalAFact.GetComponent<MeshCollider>().enabled = true;
            finalAFact.GetComponent<BoxCollider>().enabled = true;
            finalAFact.transform.parent.GetComponentInChildren<Light>().enabled = true;
            finalAFact.GetComponent<Renderer>().material = emissiveMat;
        }
    }

}
