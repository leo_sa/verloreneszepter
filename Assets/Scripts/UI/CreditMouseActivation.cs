﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;       

namespace GPPR
{
    public class CreditMouseActivation : MonoBehaviour      //written By Khira
    {
        // class that loads Main Menu after the Credits have played

        public AnimationClip CreditAnimation;
        public string myScene;
        float delayAnimation;

        // Start is called before the first frame update
        void Start()
        {
            // sets delayAnimation to length of Credit Animation
            delayAnimation = CreditAnimation.length;
        }

        // Update is called once per frame
        void Update()
        {
            // delayedTime is reduced by one every second (because of Time.deltaTime), loads Scene when delayTime is <= 0
            delayAnimation -= Time.deltaTime;
            if (delayAnimation <= 0)
            {
                SceneManager.LoadScene(myScene);
            }
        }

    }

}
