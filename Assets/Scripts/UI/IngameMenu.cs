﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class IngameMenu : MonoBehaviour         //written by Khira
    {
        // class that handles the button and input behaviour for everything in the ingame Menu

        public GameObject MenuUI, Credits, Settings, Controls, Logo;
        bool GameIsPaused;
        public AnimationClip IngameCreditAnimation;
        public Animator CreditAnimator;
        float creditDelay;
        bool creditsOn = false, settingsOn = false, controlsOn = false;

        // Start is called before the first frame update
        // makes sure Menu is switched off and time is running on start
        void Start()
        {
            MenuUI.SetActive(false);
            Resume();
        }

        // Update is called once per frame
        // handles [Esc] Behaviour and timer for Credit animation
        void Update()
        {
            // when pressing [Esc]
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // if game is paused, check if menu or submenu is open, close wathever was open 
                if (GameIsPaused)
                {
                    // close menu, if you are in menu (same as not in submenu)
                    if (creditsOn == false && settingsOn == false && controlsOn == false)
                    {
                        Resume();
                    }
                    // close submenu and open menu (when you are in submenu)
                    else
                    {
                        OpenMenu();
                    }
                }
                // if Game is playing, pause it
                else
                {
                    Pause();
                }
            }
            // if credits are on, check if delay time is still over 0, if it is, set it down by one second 
            // until it is under or equal 0, then close Credits and open Menu
            if (creditsOn == true)
            {
                if (creditDelay > 0)
                {
                    creditDelay -= Time.deltaTime;
                }
                else
                {
                    Credits.SetActive(false);
                    creditsOn = false;
                    Pause();
                }
            }

        }

        // "Pause" Game with time going on, make Cursor visible, freeze Camera, make Menu and Logo visible, turns Player movement off
        void Pause()
        {
            //Sets Cursor to  be visible
            Cursor.visible = true;
            // unlocks Cursor
            Cursor.lockState = CursorLockMode.None;
            MenuUI.SetActive(true);
            Logo.SetActive(true);
            //Time.timeScale = 0f;                          (<-- for a better feeling we decided to test it without pausing game - change if we get problems!!!)
            GameIsPaused = true;
            // gets Movement Script from Camera and switches it off
            FindObjectOfType<FPS_Mouse>().enabled = false;
            // gets Movement Script of Player and switches it off
            FindObjectOfType<FPS_Movement>().canMove = false;
        }

        // Resumes Game: closes Ingame Menu and Logo, makes time go on, locks and blocks cursor, makes camera moveable, turns player movement on
        public void Resume()
        {
            MenuUI.SetActive(false);
            Logo.SetActive(false);
            //Time.timeScale = 1f;                                            (<-- unpause Game test - change if we get problems!!!)
            GameIsPaused = false;
            //Sets Cursor to  be invisible
            Cursor.visible = false;
            // locks Cursor
            Cursor.lockState = CursorLockMode.Locked;
            // gets Movement Script from Camera and switches it on
            FindObjectOfType<FPS_Mouse>().enabled = true;
            // gets Movement Script of Player and turns it on
            FindObjectOfType<FPS_Movement>().canMove = true;
        }

        // opens Settings Panel, Closes Menu Panel, sets settings bool true
        public void OpenSettings()
        {
            MenuUI.SetActive(false);
            Settings.SetActive(true);
            settingsOn = true;
        }

        // opens Controls Panel, Closes Menu Panel, sets controls bool true
        public void OpenControls()
        {
            MenuUI.SetActive(false);
            Controls.SetActive(true);
            controlsOn = true;
        }

        // opens Credits Panel, Closes Menu Panel, sets credits bool true, sets Credit Delay witch will be counted down in Update()
        public void OpenCredits()
        {
            creditsOn = true;
            creditDelay = IngameCreditAnimation.length;
            MenuUI.SetActive(false);
            Credits.SetActive(true);
            //Time.timeScale = 1f;                                                           (<-- unpause Game test - change if we get problems!!!)
        }

        // opens Menu and closes Submenu it is in
        public void OpenMenu()
        {
            if (creditsOn)
            {
                Credits.SetActive(false);
                creditsOn = false;
            }
            if (settingsOn == true)
            {
                Settings.SetActive(false);
                settingsOn = false;
            }
            if (controlsOn == true)
            {
                Controls.SetActive(false);
                controlsOn = false;
            }
            MenuUI.SetActive(true);
        }
    }
}

