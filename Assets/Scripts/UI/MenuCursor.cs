﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class MenuCursor : MonoBehaviour     //written by Khira
    {
        // class to unlock the Cursor and make it visible (for Menu)

        // Start is called before the first frame update
        void Start()
        {
            //Sets Cursor to  be visible
            Cursor.visible = true;
            // unlocks Cursor
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 1f;
        }
    }

}
