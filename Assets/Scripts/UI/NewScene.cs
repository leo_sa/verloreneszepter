﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class NewScene : MonoBehaviour       //written by Khira
    {
        // class that Loads Scenes 

        // insert Scene to Load here
        [SerializeField]
        public string sceneName;

        // Loads Scene
        public void LoadMyScene()
        {
            SceneManager.LoadScene(sceneName);
        }
    }

}
