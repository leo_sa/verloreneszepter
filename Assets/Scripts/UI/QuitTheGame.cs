﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class QuitTheGame : MonoBehaviour        //written by Khira
    {
        // class to Quit the Game

        public void QuitGame()
        {
            Application.Quit();
            // Log Message to check if QuitTheGame() triggers in Editor
            Debug.Log("Game has been Quit.");
        }
    }

}
