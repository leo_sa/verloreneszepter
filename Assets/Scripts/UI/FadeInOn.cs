﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class FadeInOn : MonoBehaviour   //written by Khira
    {
        public GameObject fadePanel;
        int endGame;
        // Start is called before the first frame update
        void Start()
        {
            endGame = PlayerPrefs.GetInt("endGame");

            if (endGame == 1)
            {
                fadePanel.SetActive(true);
            }
            else
            {
                fadePanel.SetActive(false);
            }
            PlayerPrefs.SetInt("endGame", 0);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
