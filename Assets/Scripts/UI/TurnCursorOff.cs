﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR      //written by Khira
{
    public class TurnCursorOff : MonoBehaviour
    {
        // class to turn Cursor off (Ingame)

        // Start is called before the first frame update
        void Start()
        {
            //Sets Cursor to  be invisible
            Cursor.visible = false;
            // locks Cursor
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

}
