﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class ButtonEffects : MonoBehaviour      //written by Khira
    {
        public GameObject ButtonParticle;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void ActivateParticle()
        {
            ButtonParticle.SetActive(true);
        }
        public void DeactivateParticle()
        {
            ButtonParticle.SetActive(false);
        }
    }

}
