﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class GlobalFisch : MonoBehaviour  //written by Khira
    {
        // class that spawnes fish random in a specified tank

        public GameObject fishPrefab;
        // total number of fish
        public static int fishNumber = 100;
        // creates an array with as many slots as fishNumber is big
        public static GameObject[] allFish = new GameObject[fishNumber];
        // range (from 0/0/0) in wich fish will be spawned
        public static int tankSize = 30;
        // goal position is set to the middle of the tank, witch is at 0/0/0 now - needs to be changed later
        public static Vector3 goalPosition = Vector3.zero;
        public GameObject TankMid;

        // Start is called before the first frame update
        void Start()
        {
            // as long as i is smaller than fishNumber, do something (starting at 0, adding 1 to i every time doing it)
            for (int i = 0; i < fishNumber; i++)
            {
                // create a new Vector 3 on a random position in our tankSize where a new fish should be spawned and spawn it on this position - needs to be changed later
                Vector3 position = new Vector3(Random.Range(-tankSize, tankSize),
                                               Random.Range(-tankSize, tankSize),
                                               Random.Range(-tankSize, tankSize)) + TankMid.transform.position;
                allFish[i] = (GameObject)Instantiate(fishPrefab, position, Quaternion.identity);
            }
        }

        // Update is called once per frame
        void Update()
        {
            // every 5 in 1000 times the position of the goalPosition is changed randomly in the tank
            if (Random.Range(0, 1000) < 5)
            {
                goalPosition = new Vector3(Random.Range(-tankSize, tankSize),
                                           Random.Range(-tankSize, tankSize),
                                           Random.Range(-tankSize, tankSize)) + TankMid.transform.position;
            }
        }
    }
}

