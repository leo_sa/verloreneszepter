﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class FPS_Movement : MonoBehaviour           //Written by Leo
    {
        Rigidbody rigidbody;

        [Range(0, 1)]
        public float baseDirAcceleration, swimdecceleration = 0.4f;

        public float speedX = 4, speedY = 4;

        [SerializeField, Range(1.01f, 2f)]
        float sprintMultiplier = 1.5f;

        [SerializeField]
        float swimUpForce = 1;

        float swimHeight;

        float x, y, swinHeight;

        Transform camTransform, leaveWaterPoint;

        public bool rawAxis;

        public bool canMove;        //disabled when paused

        [SerializeField]
        bool swimming;

        bool leaveWaterbool;

        bool saveCameraVectorOnce = true;

        Vector3 lastLookDirection, lastRightDirecion;

        void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
            camTransform = Camera.main.transform;
        }


        void Update()
        {

            if (canMove)
            {
                if (!swimming) normalMove();                //In the early stages of development, a it was possible to leave the water
                else swim();
            }
            else rigidbody.velocity = new Vector3(0, 0, 0);            //stop moving when paused


            if (leaveWaterbool)
            {
                Physics.IgnoreLayerCollision(9, 10, true);
                transform.position = Vector3.Lerp(transform.position, leaveWaterPoint.position, 0.1f);

                if (Mathf.Abs(transform.position.x - leaveWaterPoint.position.x) <= 0.03f)
                {
                    swimming = false;
                    leaveWaterbool = false;
                }
            }
            else Physics.IgnoreLayerCollision(9, 10, false);

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                speedX *= sprintMultiplier;
                speedY *= sprintMultiplier;
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                speedX /= sprintMultiplier;
                speedY /= sprintMultiplier;
            }


        }


        void swim()         //swimming movement, having control of X and Z axis(Variable Naming is in this case irritating and could have been improved)
        {
            if (rawAxis)
            {
                x = Mathf.Lerp(x, Input.GetAxisRaw("Horizontal"), baseDirAcceleration);
                y = Mathf.Lerp(y, Input.GetAxisRaw("Vertical"), baseDirAcceleration);
            }
            else
            {
                x = Mathf.Lerp(x, Input.GetAxis("Horizontal"), baseDirAcceleration);
                y = Mathf.Lerp(y, Input.GetAxis("Vertical"), baseDirAcceleration);
            }

            rigidbody.MovePosition(transform.position + Vector3.forward * -0.003f);
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                rigidbody.velocity = (camTransform.right * x * speedX + camTransform.forward * y * speedY + Vector3.up * swinHeight) * Time.deltaTime * 60;     //if the player is giving movement input go this "way"
                saveCameraVectorOnce = true;        //Vector can be saved again
            }
            else
            {
                if (saveCameraVectorOnce)
                {
                    lastLookDirection = camTransform.forward;
                    lastRightDirecion = camTransform.right;
                    saveCameraVectorOnce = false;
                }
                rigidbody.velocity = (lastRightDirecion * x * speedX + lastLookDirection * y * speedY + Vector3.up * swinHeight) * Time.deltaTime * 60;    //this way the playermovement fades out in the last swim-direction, not in the current camera-look direction
            }

        }

        void normalMove()       //normal movement, but this is never used in the currrent build
        {


            x = Mathf.Lerp(x, Input.GetAxisRaw("Horizontal"), baseDirAcceleration);
            y = Mathf.Lerp(y, Input.GetAxisRaw("Vertical"), baseDirAcceleration);



            rigidbody.velocity = transform.right * x * speedX + transform.forward * y * speedY + Vector3.up * rigidbody.velocity.y;

            rigidbody.AddForce(Vector3.up * -4f);
        }

        private void OnCollisionEnter(Collision collision)                          //all onEnter methods
        {
            if (collision.gameObject.tag == "leaveWater")
            {
                leaveWaterPoint = collision.gameObject.transform.GetChild(0).transform;
                if (swimming) leaveWaterbool = true;
            }

        }


        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag == "water") swimming = true;
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "water") swimming = false;
        }


    }

}
