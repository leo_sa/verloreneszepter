﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

namespace GPPR
{
    public class GammaChange : MonoBehaviour        //written by Leo
    {

        PostProcessVolume volume;

        ColorGrading colorGrading;

        [SerializeField]
        Slider slider;

        void Start()
        {
            volume = FindObjectOfType<PostProcessVolume>();
            volume.profile.TryGetSettings<ColorGrading>(out colorGrading);
            slider.value = PlayerPrefs.GetFloat("autoEx", 1);
        }


        public void ChangeGamma()
        {
            PlayerPrefs.SetFloat("autoEx", slider.value);           //changing the Exposure Value, not gamma, by the value of the interactable slider
            PlayerPrefs.Save();
            colorGrading.postExposure.value = slider.value;
        }
    }

}
