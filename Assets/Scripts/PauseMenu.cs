﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class PauseMenu : MonoBehaviour      //written by Khira
    {
        FPS_Mouse mouseLook;

        [SerializeField]
        GameObject pauseMenuCanvas;

        bool paused;

        private void Start()
        {
            mouseLook = FindObjectOfType<FPS_Mouse>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (paused)
                {
                    mouseLook.lockMouse();
                    Time.timeScale = 1;
                    paused = false;
                }
                else if (!paused)
                {
                    mouseLook.unlockMouse();
                    Time.timeScale = 0.2f;
                    paused = true;
                }
                setMenu(paused);

                Time.fixedDeltaTime = 0.02f * Time.timeScale;
            }
        }

        void setMenu(bool set)
        {
            pauseMenuCanvas.SetActive(set);
        }
    }
}

