﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class AnimationScript : MonoBehaviour        //written by Leo, a script to do simple interpolation, to dodge the unity animator.
    {
        [SerializeField]
        float speedX, speedY, speedZ, LERP_rangeX, LERP_rangeY, LER_PrangeZ, rotationSpeedX, rotationSpeedY, rotationSpeedZ;
        [SerializeField, Range(0, 1)]
        float lerpSpeed = 0.2f, LERP_inaccuracy = 0.1f, rotationReset_accuracy = 0.3f;

        float internSpeedX, internspeedY, internSpeedZ, internLerpX, internLerpY, internLerpZ;
        float internRotX, internRotY, internRotZ;

        Vector3 originalPosition;

        bool lerpTo;

        void Start()
        {
            originalPosition = transform.position;
        }


        void Update()
        {
            LERP_Calculations();
            Rotation_Calculations();

            transform.position = Vector3.Lerp(transform.position, new Vector3(internLerpX, internLerpY, internLerpZ), lerpSpeed) + (new Vector3(internSpeedX, internspeedY, internSpeedZ));

            internSpeedX += speedX * Time.deltaTime;
            internspeedY += speedY * Time.deltaTime;
            internSpeedZ += speedZ * Time.deltaTime;
        }

        void LERP_Calculations()
        {
            if (Vector3.Distance(transform.position, new Vector3(internLerpX, internLerpY, internLerpZ)) < LERP_inaccuracy)
            {
                if (lerpTo) lerpTo = false;
                else lerpTo = true;
            }

            if (lerpTo)
            {
                internLerpX = originalPosition.x + LERP_rangeX;
                internLerpY = originalPosition.y + LERP_rangeY;
                internLerpZ = originalPosition.z + LER_PrangeZ;
            }
            else
            {
                internLerpX = originalPosition.x - LERP_rangeX;
                internLerpY = originalPosition.y - LERP_rangeY;
                internLerpZ = originalPosition.z - LER_PrangeZ;
            }
        }

        void Rotation_Calculations()
        {
            if (internRotX % 360 <= rotationReset_accuracy) internRotX = 0;
            if (internRotY % 360 <= rotationReset_accuracy) internRotY = 0;
            if (internRotZ % 360 <= rotationReset_accuracy) internRotZ = 0;

            internRotX += rotationSpeedX * Time.deltaTime;
            internRotY += rotationSpeedY * Time.deltaTime;
            internRotZ += rotationSpeedZ * Time.deltaTime;

            transform.rotation = Quaternion.Euler(internRotX, internRotY, internRotZ);
        }
    }
}

