﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class MonoBehaviour2 : MonoBehaviour         //written by Leo
    {
        public void Log(object toLog)           //Wrapping Debug.Log
        {
            Debug.Log(toLog);
        }

        void MyPlay(AudioSource source, AudioClip clip, float delay)        //to avoid repetetivity, the Pitch can be randomized
        {
            source.clip = clip;
            source.pitch = Random.Range(0.9f, 1.1f);
            source.PlayDelayed(delay);
        }
    }

}
