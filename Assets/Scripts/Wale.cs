﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class Wale : MonoBehaviour       //written by Leo
    {
        [SerializeField]
        float speed = 1;

        void Update()           //He just moves forwards.
        {
            transform.Translate(transform.forward * speed);
        }
    }

}
