﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GPPR
{

    public class ItemPickup : MonoBehaviour     //written by Leo
    {
        [SerializeField]
        LayerMask layermask;

        RaycastHit hit;

        Camera cam;

        [SerializeField]
        string pickup = "Press E";

        TextMeshProUGUI text;

        [SerializeField, Range(0, 5)]
        float length = 3;

        void Start()
        {
            cam = Camera.main;
            text = GetComponentInChildren<TextMeshProUGUI>();
            text.text = null;
        }


        void Update()
        {
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, length, layermask))           //when the raycasts hits an artefact(or the final one), execute its "remove" method and show the player the possible interaction via UI. If no artefact is in sight no UI-indicator should be visible.
            {
                if (hit.collider.tag == "artefact")
                {
                    if (Input.GetKeyDown("e")) hit.collider.gameObject.GetComponent<Artefact>().remove();
                    text.text = pickup;
                }
                else if (hit.collider.name == "FinalArtefact")
                {
                    if (Input.GetKeyDown("e")) hit.collider.gameObject.GetComponent<FinalArtefact>().remove();
                    text.text = pickup;
                }
            }
            else text.text = null;

        }
    }
}
